# README #

### What is this repository for? ###

* Backend service providing APIs for operations on widgets.

### How do I the service? ###

* To start the app run:
    * *mvnw clean install* and then
    * *mvnw spring-boot:run -Dspring-boot.run.profiles=mode* where *mode* can be *h2* or *inMemory*