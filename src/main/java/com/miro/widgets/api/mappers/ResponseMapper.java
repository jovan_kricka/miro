package com.miro.widgets.api.mappers;

import com.miro.widgets.domain.errors.DomainError;
import com.miro.widgets.domain.errors.WidgetError;
import com.miro.widgets.domain.errors.WidgetNotFoundError;
import io.vavr.control.Either;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Component
public class ResponseMapper {

    private static final Map<Class<? extends DomainError>, HttpStatus> ERROR_STATUS_DICTIONARY = Map.of(
            WidgetError.class, HttpStatus.BAD_REQUEST,
            WidgetNotFoundError.class, HttpStatus.NOT_FOUND
    );

    public <R, E extends DomainError, T> ResponseEntity<R> map(Either<E, T> result, Function<T, R> mapper, HttpStatus successfulStatus) {
        return result.fold(
                error -> ResponseEntity.status(mapErrorToHttpStatus(error)).build(),
                data -> ResponseEntity.status(successfulStatus).body(mapper.apply(data)));
    }

    public <R, T> ResponseEntity<R> mapOptional(Optional<T> result, Function<T, R> mapper) {
        return ResponseEntity.status(result.map(ignored -> HttpStatus.OK.value()).orElse(HttpStatus.NOT_FOUND.value()))
                .body(result.map(mapper).orElse(null));
    }

    private int mapErrorToHttpStatus(DomainError domainError) {
        return ERROR_STATUS_DICTIONARY.getOrDefault(domainError.getClass(), HttpStatus.INTERNAL_SERVER_ERROR).value();
    }

}
