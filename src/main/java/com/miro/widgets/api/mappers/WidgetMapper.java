package com.miro.widgets.api.mappers;

import com.miro.model.Point;
import com.miro.model.WidgetRequest;
import com.miro.model.WidgetResponse;
import com.miro.model.WidgetsPageResponse;
import com.miro.widgets.domain.models.Page;
import com.miro.widgets.domain.models.Widget;
import com.miro.widgets.domain.models.WidgetTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class WidgetMapper {

    public Widget requestToDomainModel(WidgetRequest request, UUID id) {
        return Widget.builder()
                .id(id)
                .bottomLeftPoint(com.miro.widgets.domain.models.Point.of(
                        request.getPoint().getX(),
                        request.getPoint().getY()))
                .order(request.getOrder())
                .height(request.getHeight())
                .width(request.getWidth())
                .build();
    }

    public WidgetTemplate requestToTemplateDomainModel(WidgetRequest request) {
        return WidgetTemplate.of(
                request.getPoint().getX(),
                request.getPoint().getY(),
                request.getOrder(),
                request.getHeight(),
                request.getWidth());
    }

    public WidgetResponse domainModelToResponse(Widget domainModel) {
        return new WidgetResponse()
                .point(new Point()
                        .x(domainModel.getBottomLeftPoint().getX())
                        .y(domainModel.getBottomLeftPoint().getY()))
                .order(domainModel.getOrder())
                .height(domainModel.getHeight())
                .width(domainModel.getWidth())
                .lastModificationDate(domainModel.getLastModificationDate())
                .id(domainModel.getId());
    }

    public WidgetsPageResponse domainModelPageToPaginatedResponse(Page page) {
        return new WidgetsPageResponse()
                .elements(page.getWidgets().stream().map(this::domainModelToResponse).collect(Collectors.toList()))
                .total(page.getTotal());
    }
}
