package com.miro.widgets.api.controllers;

import com.miro.api.WidgetsApi;
import com.miro.model.WidgetRequest;
import com.miro.model.WidgetResponse;
import com.miro.model.WidgetsPageResponse;
import com.miro.widgets.api.mappers.ResponseMapper;
import com.miro.widgets.api.mappers.WidgetMapper;
import com.miro.widgets.domain.models.FilteringRectangle;
import com.miro.widgets.domain.models.Page;
import com.miro.widgets.domain.ports.WidgetsInboundPort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RequiredArgsConstructor
@RestController
public class WidgetsController implements WidgetsApi {

    private final WidgetsInboundPort widgetsInboundPort;
    private final WidgetMapper widgetMapper;
    private final ResponseMapper responseMapper;

    @Override
    public ResponseEntity<WidgetResponse> createWidget(WidgetRequest widgetRequest) {
        var result = widgetsInboundPort.createWidget(widgetMapper.requestToTemplateDomainModel(widgetRequest));
        return responseMapper.map(result, widgetMapper::domainModelToResponse, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<WidgetResponse> updateWidget(UUID id, WidgetRequest widgetRequest) {
        var result = widgetsInboundPort.updateWidget(
                widgetMapper.requestToDomainModel(widgetRequest, id));
        return responseMapper.map(result, widgetMapper::domainModelToResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<WidgetResponse> getWidget(UUID id) {
        var result = widgetsInboundPort.getWidget(id);
        return responseMapper.mapOptional(result, widgetMapper::domainModelToResponse);
    }

    @Override
    public ResponseEntity<WidgetsPageResponse> getPaginated(Integer offset, Integer limit, Integer bottomLeftX, Integer bottomLeftY, Integer topRightX, Integer topRightY) {
        var filteringRectangleResult =
                FilteringRectangle.of(bottomLeftX, bottomLeftY, topRightX, topRightY);
        if (filteringRectangleResult.isLeft()) {
            return ResponseEntity.badRequest().build();
        }
        Page page = filteringRectangleResult.get()
                .map(filteringRectangle -> widgetsInboundPort.getAllFiltered(offset, limit, filteringRectangle))
                .orElse(widgetsInboundPort.getAll(offset, limit));
        var responseBuilder = ResponseEntity.status(HttpStatus.OK);
        if (page.hasNextPage()) {
            responseBuilder.header("Link", buildNextPageUri(page));
        }
        return responseBuilder.body(widgetMapper.domainModelPageToPaginatedResponse(page));
    }

    @Override
    public ResponseEntity<Void> deleteWidget(UUID id) {
        var result = widgetsInboundPort.deleteWidget(id);
        return responseMapper.map(result, ignored -> null, HttpStatus.OK);

    }

    private String buildNextPageUri(Page page) {
        URI nextPageUri = linkTo(methodOn(WidgetsController.class)
                .getPaginated(page.getNextOffset(), page.getLimit(), null, null, null, null))
                .toUri();
        return "<" + nextPageUri.toString() + ">;";

    }

}
