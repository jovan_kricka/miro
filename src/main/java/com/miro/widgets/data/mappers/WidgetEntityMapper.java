package com.miro.widgets.data.mappers;

import com.miro.widgets.data.entities.WidgetEntity;
import com.miro.widgets.domain.models.Point;
import com.miro.widgets.domain.models.Widget;
import org.springframework.stereotype.Component;

@Component
public class WidgetEntityMapper {

    public WidgetEntity domainModelToEntity(Widget widget) {
        return WidgetEntity.builder()
                .id(widget.getId())
                .bottomLeftX(widget.getBottomLeftPoint().getX())
                .bottomLeftY(widget.getBottomLeftPoint().getY())
                .orderIndex(widget.getOrder())
                .height(widget.getHeight())
                .width(widget.getWidth())
                .lastModificationDate(widget.getLastModificationDate())
                .build();
    }

    public Widget entityToDomainModel(WidgetEntity widgetEntity) {
        return Widget.builder()
                .id(widgetEntity.getId())
                .bottomLeftPoint(Point.of(widgetEntity.getBottomLeftX(), widgetEntity.getBottomLeftY()))
                .order(widgetEntity.getOrderIndex())
                .height(widgetEntity.getHeight())
                .width(widgetEntity.getWidth())
                .lastModificationDate(widgetEntity.getLastModificationDate())
                .build();
    }

}
