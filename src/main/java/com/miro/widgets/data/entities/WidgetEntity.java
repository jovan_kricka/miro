package com.miro.widgets.data.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.UUID;

@Entity(name = "widgets")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class WidgetEntity {

    @Id
    private UUID id;
    private int bottomLeftX;
    private int bottomLeftY;
    @Column(unique = true)
    private int orderIndex;
    private int height;
    private int width;
    private LocalDate lastModificationDate;

}
