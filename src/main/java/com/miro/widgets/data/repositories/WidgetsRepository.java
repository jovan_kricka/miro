package com.miro.widgets.data.repositories;

import com.miro.widgets.data.entities.WidgetEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface WidgetsRepository extends JpaRepository<WidgetEntity, UUID> {

    @Query("select max(w.orderIndex) from widgets w")
    Optional<Integer> findMaximumOrderOfAllWidgets();

    @Query("select w from widgets w")
    Page<WidgetEntity> findPage(Pageable pageable);

    List<WidgetEntity> findAllByOrderIndexGreaterThanEqual(int order);

}
