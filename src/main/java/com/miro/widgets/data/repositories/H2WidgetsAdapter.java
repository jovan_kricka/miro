package com.miro.widgets.data.repositories;

import com.miro.widgets.data.mappers.WidgetEntityMapper;
import com.miro.widgets.domain.errors.WidgetError;
import com.miro.widgets.domain.models.FilteringRectangle;
import com.miro.widgets.domain.models.Page;
import com.miro.widgets.domain.models.Widget;
import com.miro.widgets.domain.models.WidgetsBatch;
import com.miro.widgets.domain.ports.WidgetsRepositoryPort;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@Profile("h2")
@RequiredArgsConstructor
public class H2WidgetsAdapter implements WidgetsRepositoryPort {

    private final WidgetsRepository widgetsRepository;
    private final WidgetEntityMapper mapper;

    @Override
    public Optional<Widget> findWidget(UUID id) {
        return widgetsRepository.findById(id).map(mapper::entityToDomainModel);
    }

    @Override
    public Optional<Integer> findMaximumOrder() {
        return widgetsRepository.findMaximumOrderOfAllWidgets();
    }

    @Override
    public Page findAllByOrder(int offset, int limit) {
        var result = widgetsRepository.findPage(new OffsetBasedPageRequest(offset, limit));
        return Page.builder()
                .widgets(result.getContent().stream()
                        .map(mapper::entityToDomainModel)
                        .collect(Collectors.toList()))
                .total(result.getTotalElements())
                .offset(offset)
                .limit(limit)
                .build();
    }

    @Override
    public List<Widget> findAllByOrderFrom(int order) {
        return widgetsRepository.findAllByOrderIndexGreaterThanEqual(order).stream()
                .map(mapper::entityToDomainModel)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Either<WidgetError, Set<Widget>> saveWidgets(WidgetsBatch widgetsBatch) {
        Set<Widget> updatedWidgets = widgetsBatch.getConflictingWidgets().stream()
                .sorted((w1, w2) -> w2.getOrder() - w1.getOrder())
                .map(mapper::domainModelToEntity)
                .map(widgetsRepository::saveAndFlush)
                .map(mapper::entityToDomainModel)
                .collect(Collectors.toSet());
        widgetsRepository.saveAndFlush(mapper.domainModelToEntity(widgetsBatch.getModifiedWidget()));
        return Either.right(updatedWidgets);
    }

    @Override
    public Either<WidgetError, Void> deleteWidget(UUID id) {
        widgetsRepository.deleteById(id);
        return Either.right(null);
    }

    @Override
    public Page findAllByOrderAndWithinRectangle(int offset, int limit, FilteringRectangle filteringRectangle) {
        var result = widgetsRepository.findPage(new OffsetBasedPageRequest(offset, limit));
        return Page.builder()
                .widgets(filteringRectangle.apply(result.stream()
                        .map(mapper::entityToDomainModel)
                        .collect(Collectors.toList())))
                .total(result.getTotalElements())
                .offset(offset)
                .limit(limit)
                .build();
    }

    public void purge() {
        widgetsRepository.deleteAll();
    }

}
