package com.miro.widgets.data.repositories;

import com.miro.widgets.domain.errors.WidgetError;
import com.miro.widgets.domain.models.FilteringRectangle;
import com.miro.widgets.domain.models.Page;
import com.miro.widgets.domain.models.Widget;
import com.miro.widgets.domain.models.WidgetsBatch;
import com.miro.widgets.domain.ports.WidgetsRepositoryPort;
import io.vavr.control.Either;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.*;

@Profile("inMemory")
@Repository
public class InMemoryWidgetsAdapter implements WidgetsRepositoryPort {

    private final TreeMap<Integer, Widget> widgets = new TreeMap<>();

    @Override
    public Optional<Widget> findWidget(final UUID id) {
        return widgets.values().stream().filter(widget -> widget.getId().equals(id)).findFirst();
    }

    @Override
    public Optional<Integer> findMaximumOrder() {
        return widgets.isEmpty() ? Optional.empty() : Optional.of(widgets.lastKey());
    }

    @Override
    public Page findAllByOrder(int offset, int limit) {
        return Page.builder()
                .widgets(findPage(offset, limit))
                .total(widgets.size())
                .offset(offset)
                .limit(limit)
                .build();
    }

    @Override
    public Page findAllByOrderAndWithinRectangle(int offset, int limit, FilteringRectangle filteringRectangle) {
        return Page.builder()
                .widgets(filteringRectangle.apply(findPage(offset, limit)))
                .total(widgets.size())
                .offset(offset)
                .limit(limit)
                .build();
    }

    private List<Widget> findPage(int offset, int limit) {
        return new ArrayList<>(widgets.values())
                .subList(
                        Math.min(widgets.size(), offset),
                        Math.min(widgets.size(), offset + limit));

    }

    @Override
    public List<Widget> findAllByOrderFrom(int order) {
        return new ArrayList<>(widgets.tailMap(order).values());
    }

    @Override
    public Either<WidgetError, Set<Widget>> saveWidgets(WidgetsBatch widgetsBatch) {
        Set<Widget> widgetsToSave = new HashSet<>(widgetsBatch.getConflictingWidgets());
        widgetsToSave.add(widgetsBatch.getModifiedWidget());
        widgetsToSave.forEach(widget -> findWidget(widget.getId())
                .ifPresent(foundWidget -> widgets.remove(foundWidget.getOrder())));
        widgetsToSave.forEach(widget -> this.widgets.put(widget.getOrder(), widget));
        return Either.right(widgetsToSave);
    }

    @Override
    public Either<WidgetError, Void> deleteWidget(UUID id) {
        widgets.remove(findWidget(id).orElseThrow().getOrder());
        return Either.right(null);
    }

    public void purge() {
        widgets.clear();
    }

}
