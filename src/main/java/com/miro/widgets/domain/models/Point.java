package com.miro.widgets.domain.models;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor(staticName = "of")
public class Point {

    int x;
    int y;

    public boolean isRightOfOrAlignedWith(Point point) {
        return x >= point.getX();
    }

    public boolean isHigherThanOrAlignedWith(Point point) {
        return y >= point.getY();
    }

    public boolean isLeftOfOrAlignedWith(Point point) {
        return x <= point.getX();
    }

    public boolean isLowerThanOrAlignedWith(Point point) {
        return y <= point.getY();
    }

}
