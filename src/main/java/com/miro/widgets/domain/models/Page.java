package com.miro.widgets.domain.models;

import com.google.common.base.Preconditions;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
public class Page {

    List<Widget> widgets;
    long total;
    int offset;
    int limit;

    @Builder
    public Page(List<Widget> widgets, long total, int offset, int limit) {
        Preconditions.checkNotNull(widgets, "Widgets must be provided.");
        Preconditions.checkArgument(total >= 0, "Total number of widgets can not be negative.");
        Preconditions.checkArgument(offset >= 0, "Offset can not be negative.");
        Preconditions.checkArgument(limit > 0, "Limit must be greater than zero.");
        this.widgets = widgets;
        this.total = total;
        this.offset = offset;
        this.limit = limit;
    }

    public boolean hasNextPage() {
        return total > getNextOffset();
    }

    public int getNextOffset() {
        return offset + limit;
    }

}
