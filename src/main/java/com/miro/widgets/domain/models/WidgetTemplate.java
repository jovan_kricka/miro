package com.miro.widgets.domain.models;

import com.google.common.base.Preconditions;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Value;

import java.util.Optional;
import java.util.UUID;

@Value
@Builder(access = AccessLevel.PRIVATE)
public class WidgetTemplate {

    UUID id;
    Point bottomLeftPoint;
    Integer order;
    int height;
    int width;

    public static WidgetTemplate of(int x, int y, Integer order, int height, int width) {
        Preconditions.checkArgument(height > 0, "Height of the widget must be greater than zero.");
        Preconditions.checkArgument(width > 0, "Width of the widget must be greater than zero.");
        return WidgetTemplate.builder()
                .id(UUID.randomUUID())
                .bottomLeftPoint(Point.of(x, y))
                .order(order)
                .height(height)
                .width(width)
                .build();
    }

    public Widget produce(int maximumOrder) {
        return Widget.newWidget(bottomLeftPoint, getOrder().orElse(maximumOrder + 1), height, width);
    }

    public Optional<Integer> getOrder() {
        return Optional.ofNullable(order);
    }

}
