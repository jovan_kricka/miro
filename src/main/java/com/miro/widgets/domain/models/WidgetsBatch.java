package com.miro.widgets.domain.models;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
@RequiredArgsConstructor(staticName = "of")
public class WidgetsBatch {

    Widget modifiedWidget;
    List<Widget> conflictingWidgets;

}
