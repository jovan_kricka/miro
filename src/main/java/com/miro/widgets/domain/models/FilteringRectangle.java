package com.miro.widgets.domain.models;

import com.miro.widgets.domain.errors.InvalidFilteringRectangleError;
import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Value
@Builder(access = AccessLevel.PRIVATE)
public class FilteringRectangle {

    Point bottomLeftPoint;
    Point topRightPoint;

    public static Either<InvalidFilteringRectangleError, Optional<FilteringRectangle>> of(
            Integer bottomLeftX, Integer bottomLeftY, Integer topRightX, Integer topRightY) {
        if (bottomLeftX == null && bottomLeftY == null && topRightX == null && topRightY == null) {
            return Either.right(Optional.empty());
        }
        if (bottomLeftX == null || bottomLeftY == null || topRightX == null || topRightY == null) {
            return Either.left(InvalidFilteringRectangleError.of(bottomLeftX, bottomLeftY, topRightX, topRightY));
        }
        var bottomLeftPoint = Point.of(bottomLeftX, bottomLeftY);
        var topRightPoint = Point.of(topRightX, topRightY);
        if (bottomLeftPoint.isRightOfOrAlignedWith(topRightPoint) ||
                bottomLeftPoint.isHigherThanOrAlignedWith(topRightPoint)) {
            return Either.left(InvalidFilteringRectangleError.of(bottomLeftX, bottomLeftY, topRightX, topRightY));
        }
        return Either.right(Optional.of(builder()
                .bottomLeftPoint(Point.of(bottomLeftX, bottomLeftY))
                .topRightPoint(Point.of(topRightX, topRightY))
                .build()));
    }

    public List<Widget> apply(List<Widget> widgets) {
        return widgets.stream()
                .filter(this::contains)
                .collect(Collectors.toList());
    }

    private boolean contains(Widget widget) {
        return containsPoint(widget.getBottomLeftPoint()) &&
                containsPoint(widget.getTopRightPoint());

    }

    private boolean containsPoint(Point point) {
        return point.isRightOfOrAlignedWith(bottomLeftPoint) &&
                point.isHigherThanOrAlignedWith(bottomLeftPoint) &&
                point.isLeftOfOrAlignedWith(topRightPoint) &&
                point.isLowerThanOrAlignedWith(topRightPoint);

    }

}
