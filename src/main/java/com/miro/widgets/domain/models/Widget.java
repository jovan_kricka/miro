package com.miro.widgets.domain.models;

import com.google.common.base.Preconditions;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.util.UUID;

@Value
public class Widget {

    UUID id;
    Point bottomLeftPoint;
    int order;
    int height;
    int width;
    LocalDate lastModificationDate;

    @Builder(toBuilder = true)
    private Widget(UUID id, Point bottomLeftPoint, int order, int height, int width, LocalDate lastModificationDate) {
        Preconditions.checkNotNull(id, "Widget id must be provided.");
        Preconditions.checkArgument(height > 0, "Height of the widget must be greater than zero.");
        Preconditions.checkArgument(width > 0, "Width of the widget must be greater than zero.");
        this.id = id;
        this.bottomLeftPoint = bottomLeftPoint;
        this.order = order;
        this.height = height;
        this.width = width;
        this.lastModificationDate = lastModificationDate == null ? LocalDate.now() : lastModificationDate;
    }

    public static Widget newWidget(Point bottomLeftPoint, int order, int height, int width) {
        Preconditions.checkNotNull(bottomLeftPoint, "Bottom left point must be provided.");
        Preconditions.checkArgument(height > 0, "Height of the widget must be greater than zero.");
        Preconditions.checkArgument(width > 0, "Width of the widget must be greater than zero.");
        return Widget.builder()
                .id(UUID.randomUUID())
                .bottomLeftPoint(bottomLeftPoint)
                .order(order)
                .height(height)
                .width(width)
                .build();
    }

    public Widget bumpUp() {
        return toBuilder()
                .order(order + 1)
                .build();
    }

    public Point getTopRightPoint() {
        return Point.of(bottomLeftPoint.getX() + width, bottomLeftPoint.getY() + height);
    }

}
