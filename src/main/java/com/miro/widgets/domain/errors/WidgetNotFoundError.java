package com.miro.widgets.domain.errors;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.UUID;

@Value
@EqualsAndHashCode(callSuper = true)
@RequiredArgsConstructor(staticName = "of")
public class WidgetNotFoundError extends WidgetError {

    UUID id;

    @Override
    public String toString() {
        return String.format("Widget with id %s was not found.", id);
    }

}
