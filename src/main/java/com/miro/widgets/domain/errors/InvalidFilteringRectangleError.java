package com.miro.widgets.domain.errors;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(access = AccessLevel.PRIVATE)
public class InvalidFilteringRectangleError implements DomainError {

    Integer bottomLeftX;
    Integer bottomLeftY;
    Integer topRightX;
    Integer topRightY;

    public static InvalidFilteringRectangleError of(Integer bottomLeftX, Integer bottomLeftY, Integer topRightX, Integer topRightY) {
        return InvalidFilteringRectangleError.builder()
                .bottomLeftX(bottomLeftX)
                .bottomLeftY(bottomLeftY)
                .topRightX(topRightX)
                .topRightY(topRightY)
                .build();
    }

    @Override
    public String toString() {
        return String.format("Invalid filtering rectangle coordinates [%s, %s] and [%s, %s]"
                , bottomLeftX, bottomLeftY, topRightX, topRightY);
    }

}
