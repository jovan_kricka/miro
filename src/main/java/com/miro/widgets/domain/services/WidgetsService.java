package com.miro.widgets.domain.services;

import com.miro.widgets.domain.errors.WidgetError;
import com.miro.widgets.domain.errors.WidgetNotFoundError;
import com.miro.widgets.domain.models.*;
import com.miro.widgets.domain.ports.WidgetsInboundPort;
import com.miro.widgets.domain.ports.WidgetsRepositoryPort;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@Service
@Validated
public class WidgetsService implements WidgetsInboundPort {

    private final WidgetsRepositoryPort widgetsRepository;
    private final ReadWriteLock lock;
    private final int maxPaginationLimit;

    public WidgetsService(WidgetsRepositoryPort widgetsRepository,
                          @Value("${widgets.max-pagination-limit:500}") int maxPaginationLimit) {
        this.widgetsRepository = widgetsRepository;
        this.lock = new ReentrantReadWriteLock(true);
        this.maxPaginationLimit = maxPaginationLimit;
    }

    @Override
    public Either<WidgetError, Widget> createWidget(final WidgetTemplate widgetTemplate) {
        try {
            acquireWriteLock();
            Widget newWidget = widgetTemplate.produce(widgetsRepository.findMaximumOrder().orElse(0));
            return saveAndBumpUpConflictingWidgets(newWidget);
        } finally {
            releaseWriteLock();
        }
    }

    @Override
    public Either<WidgetError, Widget> updateWidget(Widget widget) {
        try {
            acquireWriteLock();
            var existingWidget = widgetsRepository.findWidget(widget.getId());
            if (existingWidget.isEmpty()) {
                return Either.left(WidgetNotFoundError.of(widget.getId()));
            }
            return saveAndBumpUpConflictingWidgets(widget);
        } finally {
            releaseWriteLock();
        }
    }

    private Either<WidgetError, Widget> saveAndBumpUpConflictingWidgets(Widget widget) {
        List<Widget> conflictingWidgets = findConflictingWidgets(widget).stream()
                .map(Widget::bumpUp)
                .collect(Collectors.toList());
        return widgetsRepository.saveWidgets(WidgetsBatch.of(widget, conflictingWidgets)).map(ignored -> widget);
    }

    @Nonnull
    private List<Widget> findConflictingWidgets(Widget newWidget) {
        List<Widget> potentialConflictingWidgets = widgetsRepository.findAllByOrderFrom(newWidget.getOrder());
        List<Widget> conflictingWidgets = new ArrayList<>();
        for (int i = 0; i < potentialConflictingWidgets.size(); i++) {
            Widget potentialConflictingWidget = potentialConflictingWidgets.get(i);
            if (potentialConflictingWidget.getOrder() == newWidget.getOrder() + i) {
                conflictingWidgets.add(potentialConflictingWidget);
            } else {
                break;
            }
        }
        return conflictingWidgets;
    }

    @Override
    public Page getAll(int offset, int limit) {
        try {
            acquireReadLock();
            return widgetsRepository.findAllByOrder(offset, sanitizeLimit(limit));
        } finally {
            releaseReadLock();
        }
    }

    @Override
    public Page getAllFiltered(int offset, int limit, FilteringRectangle filteringRectangle) {
        try {
            acquireReadLock();
            return widgetsRepository.findAllByOrderAndWithinRectangle(offset, sanitizeLimit(limit), filteringRectangle);
        } finally {
            releaseReadLock();
        }
    }

    @Override
    public Either<WidgetError, Void> deleteWidget(UUID id) {
        try {
            acquireWriteLock();
            var existingWidget = widgetsRepository.findWidget(id);
            if (existingWidget.isEmpty()) {
                return Either.left(WidgetNotFoundError.of(id));
            }
            return widgetsRepository.deleteWidget(id);
        } finally {
            releaseWriteLock();
        }
    }

    @Override
    public Optional<Widget> getWidget(UUID id) {
        try {
            acquireReadLock();
            return widgetsRepository.findWidget(id);
        } finally {
            releaseReadLock();
        }
    }

    @Override
    public int sanitizeLimit(int limit) {
        return Math.min(limit, maxPaginationLimit);
    }

    private void acquireWriteLock() {
        lock.writeLock().lock();
    }

    private void acquireReadLock() {
        lock.readLock().lock();
    }

    private void releaseWriteLock() {
        lock.writeLock().unlock();
    }

    private void releaseReadLock() {
        lock.readLock().unlock();
    }

}
