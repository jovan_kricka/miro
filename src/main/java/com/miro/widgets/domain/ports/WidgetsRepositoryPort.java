package com.miro.widgets.domain.ports;

import com.miro.widgets.domain.errors.WidgetError;
import com.miro.widgets.domain.models.FilteringRectangle;
import com.miro.widgets.domain.models.Page;
import com.miro.widgets.domain.models.Widget;
import com.miro.widgets.domain.models.WidgetsBatch;
import io.vavr.control.Either;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface WidgetsRepositoryPort {

    Optional<Widget> findWidget(UUID id);

    Optional<Integer> findMaximumOrder();

    Page findAllByOrder(int offset, int limit);

    List<Widget> findAllByOrderFrom(int order);

    Either<WidgetError, Set<Widget>> saveWidgets(WidgetsBatch widgetsBatch);

    Either<WidgetError, Void> deleteWidget(UUID id);

    Page findAllByOrderAndWithinRectangle(int offset, int limit, FilteringRectangle filteringRectangle);
}
