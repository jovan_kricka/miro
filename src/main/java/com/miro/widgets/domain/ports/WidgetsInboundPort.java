package com.miro.widgets.domain.ports;

import com.miro.widgets.domain.errors.WidgetError;
import com.miro.widgets.domain.models.FilteringRectangle;
import com.miro.widgets.domain.models.Page;
import com.miro.widgets.domain.models.Widget;
import com.miro.widgets.domain.models.WidgetTemplate;
import io.vavr.control.Either;

import java.util.Optional;
import java.util.UUID;

public interface WidgetsInboundPort {

    Either<WidgetError, Widget> createWidget(WidgetTemplate widgetTemplate);

    Either<WidgetError, Widget> updateWidget(Widget widgetTemplate);

    Page getAll(int offset, int limit);

    Page getAllFiltered(int offset, int limit, FilteringRectangle filteringRectangle);

    Either<WidgetError, Void> deleteWidget(UUID id);

    Optional<Widget> getWidget(UUID id);

    int sanitizeLimit(int limit);
}
