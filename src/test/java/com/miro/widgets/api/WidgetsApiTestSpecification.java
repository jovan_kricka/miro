package com.miro.widgets.api;

import com.miro.model.Point;
import com.miro.model.WidgetRequest;
import com.miro.model.WidgetResponse;
import com.miro.model.WidgetsPageResponse;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static com.miro.widgets.util.ApiUtils.givenWidgetsApi;
import static com.miro.widgets.util.Predicates.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
abstract class WidgetsApiTestSpecification {

    @LocalServerPort
    private int port;

    @BeforeEach
    void setup() {
        RestAssured.reset();
        RestAssured.port = port;
        cleanUp();
    }

    protected abstract void cleanUp();

    @Test
    public void successfullyCreateValidWidget() {
        // @formatter:off
        var response = givenWidgetsApi()
                .body(Fixtures.VALID_WIDGET_REQUEST)
            .when()
                .post()
            .then()
                .statusCode(HttpStatus.CREATED.value())
                .extract()
                .body()
                .as(WidgetResponse.class);
        // @formatter:on

        assertThat(response)
                .isNotNull();
        assertThat(response)
                .matches(widgetRequest(Fixtures.VALID_WIDGET_REQUEST));
    }

    @Test
    public void failCreatingWidgetWithNoXCoordinate() {
        // @formatter:off
        givenWidgetsApi()
                .body(Fixtures.WIDGET_REQUEST_WITH_NO_X_COORDINATE)
            .when()
                .post()
            .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        // @formatter:on
    }

    @Test
    public void failCreatingWidgetWithNoYCoordinate() {
        // @formatter:off
        givenWidgetsApi()
                .body(Fixtures.WIDGET_REQUEST_WITH_NO_Y_COORDINATE)
            .when()
                .post()
            .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        // @formatter:on
    }

    @Test
    public void successfullyCreateWidgetWithDefaultOrder() {
        // @formatter:off
        givenWidgetsApi()
                .body(Fixtures.WIDGET_REQUEST_WITH_NO_ORDER)
            .when()
                .post()
            .then()
                .statusCode(HttpStatus.CREATED.value());
        // @formatter:on
    }

    @Test
    public void failCreatingWidgetWithNoHeight() {
        // @formatter:off
        givenWidgetsApi()
                .body(Fixtures.WIDGET_REQUEST_WITH_NO_HEIGHT)
            .when()
                .post()
            .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        // @formatter:on
    }

    @Test
    public void failCreatingWidgetWithNoWidth() {
        // @formatter:off
        givenWidgetsApi()
                .body(Fixtures.WIDGET_REQUEST_WITH_NO_WIDTH)
            .when()
                .post()
            .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        // @formatter:on
    }

    @Test
    public void bumpUpExistingWidgetWithConflictingOrderWhenCreatingNewWidget() {
        createWidget(Fixtures.VALID_WIDGET_REQUEST.order(42));
        createWidget(Fixtures.VALID_WIDGET_REQUEST.order(42));

        // @formatter:off
        var widgetPage = givenWidgetsApi()
            .when()
                .get()
            .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .as(WidgetsPageResponse.class);
        // @formatter:on

        assertThat(widgetPage.getTotal())
                .isEqualTo(2);
        assertThat(widgetPage.getElements())
                .areExactly(1, withOrder(42))
                .areExactly(1, withOrder(43));
    }

    @Test
    public void successfullyCreateAndFetchWidgets() {
        WidgetResponse firstWidget = createWidget(Fixtures.VALID_WIDGET_REQUEST);
        WidgetResponse secondWidget = createWidget(Fixtures.VALID_WIDGET_REQUEST);

        // @formatter:off
        var widgetPage = givenWidgetsApi()
            .when()
                .get()
            .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .as(WidgetsPageResponse.class);
        // @formatter:on

        assertThat(widgetPage.getTotal())
                .isEqualTo(2);
        assertThat(widgetPage.getElements())
                .areExactly(1, withId(firstWidget.getId()))
                .areExactly(1, withId(secondWidget.getId()));
    }

    @Test
    public void successfullyReceiveLinkToNextPageInResponseHeader() {
        createWidget(Fixtures.VALID_WIDGET_REQUEST);
        createWidget(Fixtures.VALID_WIDGET_REQUEST);

        // @formatter:off
        var nextPageLink = givenWidgetsApi()
            .when()
                .get("?offset=0&limit=1")
            .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .header("Link");
        // @formatter:on

        assertThat(nextPageLink)
                .isNotNull()
                .endsWith("/widgets?offset=1&limit=1>;");
    }

    @Test
    public void successfullyReceiveLinkWithSanitizedLimitWhenGivenLimitIsTooLarge() {
        createWidget(Fixtures.VALID_WIDGET_REQUEST);
        createWidget(Fixtures.VALID_WIDGET_REQUEST);
        createWidget(Fixtures.VALID_WIDGET_REQUEST);
        createWidget(Fixtures.VALID_WIDGET_REQUEST);

        // @formatter:off
        var nextPageLink = givenWidgetsApi()
            .when()
                .get("?offset=0&limit=42")
            .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .header("Link");
        // @formatter:on

        assertThat(nextPageLink)
                .isNotNull()
                .endsWith("/widgets?offset=2&limit=2>;");
    }

    @Test
    public void successfullyUpdateWidget() {
        WidgetResponse widget = createWidget(Fixtures.VALID_WIDGET_REQUEST);
        WidgetRequest widgetUpdateRequest = Fixtures.VALID_WIDGET_REQUEST.width(1989);

        // @formatter:off
        var widgetResponse = givenWidgetsApi()
                .body(widgetUpdateRequest)
            .when()
                .put("/" + widget.getId())
            .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .as(WidgetResponse.class);
        // @formatter:on

        assertThat(widgetResponse)
                .isNotNull();
        assertThat(widgetResponse.getHeight())
                .isEqualTo(widgetUpdateRequest.getHeight());
    }

    @Test
    public void failUpdatingNonExistingWidget() {
        WidgetRequest widgetUpdateRequest = Fixtures.VALID_WIDGET_REQUEST;

        // @formatter:off
        givenWidgetsApi()
                .body(widgetUpdateRequest)
            .when()
                .put("/" + UUID.randomUUID().toString())
            .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
        // @formatter:on
    }

    @Test
    public void successfullyFetchExistingWidget() {
        WidgetResponse widget = createWidget(Fixtures.VALID_WIDGET_REQUEST);

        // @formatter:off
        var widgetResponse = givenWidgetsApi()
            .when()
                .get("/" + widget.getId())
            .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .as(WidgetResponse.class);
        // @formatter:on

        assertThat(widgetResponse)
                .isNotNull();
        assertThat(widgetResponse)
                .matches(widgetRequest(Fixtures.VALID_WIDGET_REQUEST));
    }

    @Test
    public void failFetchingNonExistingWidget() {
        // @formatter:off
        givenWidgetsApi()
            .when()
                .get("/" + UUID.randomUUID().toString())
            .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
        // @formatter:on
    }

    @Test
    public void successfullyDeleteExistingWidget() {
        WidgetResponse widget = createWidget(Fixtures.VALID_WIDGET_REQUEST);

        // @formatter:off
        givenWidgetsApi()
            .when()
                .delete("/" + widget.getId())
            .then()
                .statusCode(HttpStatus.OK.value());
        // @formatter:on
    }

    @Test
    public void failDeletingNonExistingWidget() {
        // @formatter:off
        givenWidgetsApi()
            .when()
                .delete("/" + UUID.randomUUID().toString())
            .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
        // @formatter:on
    }

    @Test
    public void successfullyFilterOutWidgets() {
        WidgetResponse firstWidget = createWidget(Fixtures.WIDGET_REQUEST_0_0);
        createWidget(Fixtures.WIDGET_REQUEST_0_50);

        // @formatter:off
        var widgetsPage = givenWidgetsApi()
            .when()
                .get("?bottomLeftX=0&bottomLeftY=0&topRightX=100&topRightY=100")
            .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .as(WidgetsPageResponse.class);
        // @formatter:on

        assertThat(widgetsPage.getElements())
                .hasSize(1)
                .containsExactly(firstWidget);
    }

    private WidgetResponse createWidget(WidgetRequest widgetRequest) {
        // @formatter:off
        return givenWidgetsApi()
                .body(widgetRequest)
            .when()
                .post()
            .then()
                .statusCode(HttpStatus.CREATED.value())
                .extract()
                .body()
                .as(WidgetResponse.class);
        // @formatter:on
    }

    interface Fixtures {

        WidgetRequest VALID_WIDGET_REQUEST = new WidgetRequest()
                .point(new Point().x(42).y(42))
                .order(0)
                .height(20)
                .width(20);
        WidgetRequest WIDGET_REQUEST_WITH_NO_X_COORDINATE = new WidgetRequest()
                .point(new Point().y(42))
                .order(0)
                .height(20)
                .width(20);
        WidgetRequest WIDGET_REQUEST_WITH_NO_Y_COORDINATE = new WidgetRequest()
                .point(new Point().x(42))
                .order(0)
                .height(20)
                .width(20);
        WidgetRequest WIDGET_REQUEST_WITH_NO_ORDER = new WidgetRequest()
                .point(new Point().x(42).y(42))
                .height(20)
                .width(20);
        WidgetRequest WIDGET_REQUEST_WITH_NO_HEIGHT = new WidgetRequest()
                .point(new Point().x(42).y(42))
                .order(0)
                .width(20);
        WidgetRequest WIDGET_REQUEST_WITH_NO_WIDTH = new WidgetRequest()
                .point(new Point().x(42).y(42))
                .order(0)
                .height(20);
        WidgetRequest WIDGET_REQUEST_0_0 = new WidgetRequest()
                .point(new Point().x(0).y(0))
                .order(1)
                .height(100)
                .width(100);
        WidgetRequest WIDGET_REQUEST_0_50 = new WidgetRequest()
                .point(new Point().x(0).y(50))
                .order(2)
                .height(100)
                .width(100);

    }

}
