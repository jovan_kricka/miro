package com.miro.widgets.api;

import com.miro.widgets.data.repositories.H2WidgetsAdapter;
import com.miro.widgets.data.repositories.InMemoryWidgetsAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("h2")
public class H2WidgetApiTest extends WidgetsApiTestSpecification {

    @Autowired
    private H2WidgetsAdapter widgetsRepository;

    @Override
    protected void cleanUp() {
        widgetsRepository.purge();
    }

}
