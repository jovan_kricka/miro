package com.miro.widgets.api;

import com.miro.widgets.data.repositories.InMemoryWidgetsAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("inMemory")
public class InMemoryWidgetApiTest extends WidgetsApiTestSpecification {

    @Autowired
    private InMemoryWidgetsAdapter widgetsRepository;

    @Override
    protected void cleanUp() {
        widgetsRepository.purge();
    }

}
