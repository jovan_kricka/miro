package com.miro.widgets.api.mappers;

import com.miro.model.Point;
import com.miro.model.WidgetRequest;
import com.miro.model.WidgetResponse;
import com.miro.widgets.domain.models.Widget;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class WidgetMapperTest {

    private final WidgetMapper widgetMapper = new WidgetMapper();

    @Test
    void mapWidgetRequestToDomainModelSuccessfully() {
        var widgetTemplate = widgetMapper.requestToTemplateDomainModel(Fixtures.WIDGET_REQUEST);

        assertThat(widgetTemplate.getBottomLeftPoint().getX()).isEqualTo(Fixtures.WIDGET_REQUEST.getPoint().getX());
        assertThat(widgetTemplate.getBottomLeftPoint().getY()).isEqualTo(Fixtures.WIDGET_REQUEST.getPoint().getY());
        assertThat(widgetTemplate.getOrder()).hasValue(Fixtures.WIDGET_REQUEST.getOrder());
        assertThat(widgetTemplate.getHeight()).isEqualTo(Fixtures.WIDGET_REQUEST.getHeight());
        assertThat(widgetTemplate.getWidth()).isEqualTo(Fixtures.WIDGET_REQUEST.getWidth());
        assertThat(widgetTemplate.getId()).isNotNull();
    }

    @Test
    void mapWidgetDomainModelToWidgetResponse() {
        WidgetResponse widgetResponse = widgetMapper.domainModelToResponse(Fixtures.WIDGET);

        assertThat(widgetResponse).isNotNull();
        assertThat(widgetResponse.getPoint().getX()).isEqualTo(Fixtures.WIDGET.getBottomLeftPoint().getX());
        assertThat(widgetResponse.getPoint().getY()).isEqualTo(Fixtures.WIDGET.getBottomLeftPoint().getY());
        assertThat(widgetResponse.getOrder()).isEqualTo(Fixtures.WIDGET.getOrder());
        assertThat(widgetResponse.getHeight()).isEqualTo(Fixtures.WIDGET.getHeight());
        assertThat(widgetResponse.getWidth()).isEqualTo(Fixtures.WIDGET.getWidth());
        assertThat(widgetResponse.getId()).isEqualTo(Fixtures.WIDGET.getId());
    }

    interface Fixtures {

        WidgetRequest WIDGET_REQUEST = new WidgetRequest()
                .point(new Point().x(42).y(42))
                .order(0)
                .height(20)
                .width(20);
        Widget WIDGET = Widget.newWidget(com.miro.widgets.domain.models.Point.of(42, 42), 0, 20, 20);

    }

}
