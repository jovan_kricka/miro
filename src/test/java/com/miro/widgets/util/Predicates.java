package com.miro.widgets.util;

import com.miro.model.WidgetRequest;
import com.miro.model.WidgetResponse;
import com.miro.widgets.domain.models.Widget;
import com.miro.widgets.domain.models.WidgetTemplate;
import org.assertj.core.api.Condition;

import java.util.UUID;
import java.util.function.Predicate;

public class Predicates {

    public static Predicate<? super WidgetResponse> widgetRequest(WidgetRequest request) {
        return (Predicate<WidgetResponse>) response -> response.getPoint().getX().equals(request.getPoint().getX()) &&
                response.getPoint().getY().equals(request.getPoint().getY()) &&
                response.getOrder().equals(request.getOrder()) &&
                response.getHeight().equals(request.getHeight()) &&
                response.getWidth().equals(request.getWidth()) &&
                response.getId() != null &&
                response.getLastModificationDate() != null;
    }

    public static Predicate<? super Widget> widgetTemplate(WidgetTemplate widgetTemplate) {
        return (Predicate<Widget>) widget -> widget.getBottomLeftPoint().equals(widgetTemplate.getBottomLeftPoint()) &&
                widget.getWidth() == widgetTemplate.getWidth() &&
                widget.getHeight() == widgetTemplate.getHeight();
    }

    public static Predicate<? super Widget> widget(Widget widgetMatch) {
        return (Predicate<Widget>) widget -> widget.getBottomLeftPoint().equals(widgetMatch.getBottomLeftPoint()) &&
                widget.getOrder() == widgetMatch.getOrder() &&
                widget.getWidth() == widgetMatch.getWidth() &&
                widget.getHeight() == widgetMatch.getHeight() &&
                widget.getId() == widgetMatch.getId() &&
                widget.getLastModificationDate() == widgetMatch.getLastModificationDate();
    }


    public static Condition<WidgetResponse> withOrder(int order) {
        return new Condition<>(widget -> widget.getOrder().equals(order), "Widget order matches " + order);
    }

    public static Condition<WidgetResponse> withId(UUID id) {
        return new Condition<>(widget -> widget.getId().equals(id), "Widget id matches " + id);
    }

    public static Condition<Widget> matching(Widget widgetMatch) {
        return new Condition<>(widget -> widget.getBottomLeftPoint().equals(widgetMatch.getBottomLeftPoint()) &&
                widget.getOrder() == widgetMatch.getOrder() &&
                widget.getWidth() == widgetMatch.getWidth() &&
                widget.getHeight() == widgetMatch.getHeight() &&
                widget.getId() == widgetMatch.getId() &&
                widgetMatch.getLastModificationDate() == widgetMatch.getLastModificationDate(),
                "Widget matches " + widgetMatch);
    }

}
