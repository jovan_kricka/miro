package com.miro.widgets.util;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class ApiUtils {

    public static RequestSpecification givenWidgetsApi() {
        return given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .basePath("/widgets");
    }

}
