package com.miro.widgets.domain.services;

import com.miro.widgets.data.repositories.InMemoryWidgetsAdapter;
import com.miro.widgets.domain.errors.WidgetNotFoundError;
import com.miro.widgets.domain.models.Point;
import com.miro.widgets.domain.models.Widget;
import com.miro.widgets.domain.models.WidgetTemplate;
import com.miro.widgets.domain.ports.WidgetsInboundPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static com.miro.widgets.util.Predicates.*;
import static org.assertj.core.api.Assertions.assertThat;

class WidgetServiceTest {

    private final InMemoryWidgetsAdapter widgetsRepository = new InMemoryWidgetsAdapter();
    private final WidgetsInboundPort widgetsPort = new WidgetsService(widgetsRepository, 500);

    @BeforeEach
    void setup() {
        widgetsRepository.purge();
    }

    @Test
    void successfullyCreateWidgetFromWidgetTemplate() {
        var result = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE);

        assertThat(result.isRight())
                .isTrue();
        assertThat(result.get())
                .matches(widgetTemplate(Fixtures.WIDGET_TEMPLATE));
    }

    @Test
    void successfullyUpdateExistingWidget() {
        var widget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE).get();
        var widgetUpdate = widget.bumpUp();

        var result = widgetsPort.updateWidget(widgetUpdate);

        assertThat(result.isRight())
                .isTrue();
        assertThat(result.get())
                .matches(widget(widgetUpdate));
    }

    @Test
    void failUpdatingNonExistingWidget() {
        var result = widgetsPort.updateWidget(Fixtures.NEW_WIDGET);

        assertThat(result.isLeft())
                .isTrue();
        assertThat(result.getLeft())
                .isInstanceOf(WidgetNotFoundError.class);
    }

    @Test
    void successfullyGetExistingWidget() {
        var existingWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE).get();

        var fetchedWidget = widgetsPort.getWidget(existingWidget.getId());

        assertThat(fetchedWidget)
                .isPresent()
                .hasValueSatisfying(matching(existingWidget));
    }

    @Test
    void successfullyGetExistingWidgetsOrderedByOrder() {
        var firstWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_THREE).get();
        var secondWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_FIVE).get();
        var thirdWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_SIX).get();
        var fourthWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_EIGHT).get();

        var fetchedWidgets = widgetsPort.getAll(0, Integer.MAX_VALUE);

        assertThat(fetchedWidgets.getTotal())
                .isEqualTo(4);
        assertThat(fetchedWidgets.getWidgets())
                .hasSize(4)
                .containsExactly(firstWidget, secondWidget, thirdWidget, fourthWidget);
    }

    @Test
    void successfullyBumpUpConflictingWidgetsWhenCreatingNewWidget() {
        var firstWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_FIVE).get();
        var secondWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_SIX).get();

        var newWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_FIVE).get();

        var fetchedWidgets = widgetsPort.getAll(0, Integer.MAX_VALUE).getWidgets();
        assertThat(fetchedWidgets)
                .hasSize(3)
                .areExactly(1, matching(firstWidget.bumpUp()))
                .areExactly(1, matching(secondWidget.bumpUp()))
                .areExactly(1, matching(newWidget));
    }

    @Test
    void successfullyBumpUpOnlyConflictingWidgetsWhenCreatingNewWidget() {
        var firstWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_FIVE).get();
        var secondWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_SIX).get();
        var thirdWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_EIGHT).get();

        var newWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_FIVE).get();

        var fetchedWidgets = widgetsPort.getAll(0, Integer.MAX_VALUE).getWidgets();
        assertThat(fetchedWidgets)
                .hasSize(4)
                .areExactly(1, matching(firstWidget.bumpUp()))
                .areExactly(1, matching(secondWidget.bumpUp()))
                .areExactly(1, matching(thirdWidget))
                .areExactly(1, matching(newWidget));
    }

    @Test
    void successfullyBumpUpConflictingWidgetsWhenUpdatingExistingWidget() {
        var firstWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_THREE).get();
        var secondWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_SIX).get();
        var firstWidgetUpdate = firstWidget.toBuilder().order(6).build();

        var updatedFirstWidget = widgetsPort.updateWidget(firstWidgetUpdate).get();

        var fetchedWidgets = widgetsPort.getAll(0, Integer.MAX_VALUE).getWidgets();
        assertThat(fetchedWidgets)
                .hasSize(2)
                .areExactly(1, matching(updatedFirstWidget))
                .areExactly(1, matching(secondWidget.bumpUp()));
    }

    @Test
    void successfullyBumpUpOnlyConflictingWidgetsWhenUpdatingExistingWidget() {
        var firstWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_THREE).get();
        var secondWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_SIX).get();
        var thirdWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE_WITH_ORDER_EIGHT).get();
        var firstWidgetUpdate = firstWidget.toBuilder().order(6).build();

        var updatedFirstWidget = widgetsPort.updateWidget(firstWidgetUpdate).get();

        var fetchedWidgets = widgetsPort.getAll(0, Integer.MAX_VALUE).getWidgets();
        assertThat(fetchedWidgets)
                .hasSize(3)
                .areExactly(1, matching(updatedFirstWidget))
                .areExactly(1, matching(secondWidget.bumpUp()))
                .areExactly(1, matching(thirdWidget));
    }

    @Test
    void successfullyDeleteExistingWidget() {
        var existingWidget = widgetsPort.createWidget(Fixtures.WIDGET_TEMPLATE).get();

        var result = widgetsPort.deleteWidget(existingWidget.getId());

        assertThat(result.isRight())
                .isTrue();
        var fetchedWidget = widgetsPort.getWidget(existingWidget.getId());
        assertThat(fetchedWidget)
                .isEmpty();
    }

    @Test
    void failDeletingNonExistingWidget() {
        var result = widgetsPort.deleteWidget(UUID.randomUUID());

        assertThat(result.isLeft())
                .isTrue();
        assertThat(result.getLeft())
                .isInstanceOf(WidgetNotFoundError.class);
    }

    interface Fixtures {

        WidgetTemplate WIDGET_TEMPLATE = WidgetTemplate.of(1, 2, 1, 20, 20);
        WidgetTemplate WIDGET_TEMPLATE_WITH_ORDER_THREE = WidgetTemplate.of(1, 2, 3, 20, 20);
        WidgetTemplate WIDGET_TEMPLATE_WITH_ORDER_FIVE = WidgetTemplate.of(1, 2, 5, 20, 20);
        WidgetTemplate WIDGET_TEMPLATE_WITH_ORDER_SIX = WidgetTemplate.of(1, 2, 6, 20, 20);
        WidgetTemplate WIDGET_TEMPLATE_WITH_ORDER_EIGHT = WidgetTemplate.of(1, 2, 8, 20, 20);
        Widget NEW_WIDGET = Widget.newWidget(Point.of(1, 2), 1, 20, 20);

    }

}
