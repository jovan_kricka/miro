package com.miro.widgets.domain.models;

import com.miro.widgets.domain.errors.InvalidFilteringRectangleError;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class FilteringRectangleTest {

    @Test
    void successfullyCreateFilteringRectangle() {
        var result = FilteringRectangle.of(1, 1, 5, 5);

        assertThat(result.isRight())
                .isTrue();
        Optional<FilteringRectangle> maybeFilteringRectangle = result.get();
        assertThat(maybeFilteringRectangle)
                .isPresent();
        FilteringRectangle filteringRectangle = maybeFilteringRectangle.get();
        assertThat(filteringRectangle.getBottomLeftPoint())
                .isEqualTo(Point.of(1, 1));
        assertThat(filteringRectangle.getTopRightPoint())
                .isEqualTo(Point.of(5, 5));
    }

    @Test
    void returnNoFilteringRectangleIfNoParametersProvided() {
        var result = FilteringRectangle.of(null, null, null, null);

        assertThat(result.isRight())
                .isTrue();
        assertThat(result.get())
                .isEmpty();
    }

    @Test
    void returnInvalidFilteringRectangleErrorIfPartialParametersProvided() {
        var result = FilteringRectangle.of(1, null, null, null);

        assertThat(result.isLeft())
                .isTrue();
        assertThat(result.getLeft())
                .isInstanceOf(InvalidFilteringRectangleError.class);
    }

    @Test
    void returnInvalidFilteringRectangleErrorIfBottomLeftPointIsAlignedVerticallyWithTopRightPoint() {
        var result = FilteringRectangle.of(1, 1, 1, 5);

        assertThat(result.isLeft())
                .isTrue();
        assertThat(result.getLeft())
                .isInstanceOf(InvalidFilteringRectangleError.class);
    }

    @Test
    void returnInvalidFilteringRectangleErrorIfBottomLeftPointIsRightOfTopRightPoint() {
        var result = FilteringRectangle.of(5, 1, 1, 5);

        assertThat(result.isLeft())
                .isTrue();
        assertThat(result.getLeft())
                .isInstanceOf(InvalidFilteringRectangleError.class);
    }

    @Test
    void returnInvalidFilteringRectangleErrorIfBottomLeftPointIsAlignedHorizontallyWithTopRightPoint() {
        var result = FilteringRectangle.of(1, 1, 5, 1);

        assertThat(result.isLeft())
                .isTrue();
        assertThat(result.getLeft())
                .isInstanceOf(InvalidFilteringRectangleError.class);
    }

    @Test
    void returnInvalidFilteringRectangleErrorIfBottomLeftPointIsHigherThanTopRightPoint() {
        var result = FilteringRectangle.of(1, 5, 5, 3);

        assertThat(result.isLeft())
                .isTrue();
        assertThat(result.getLeft())
                .isInstanceOf(InvalidFilteringRectangleError.class);
    }

    @Test
    void successfullyFilterWidgetsContainedInTheFilteringRectangle() {
        var filteringRectangle = FilteringRectangle.of(0, 0, 100, 150).get().orElseThrow();

        var filteredWidgets = filteringRectangle.apply(List.of(
                Fixtures.WIDGET_50_50,
                Fixtures.WIDGET_50_100,
                Fixtures.WIDGET_100_100));

        assertThat(filteredWidgets)
                .hasSize(2)
                .containsExactlyInAnyOrder(Fixtures.WIDGET_50_50, Fixtures.WIDGET_50_100);
    }

    @Test
    void successfullyFilterWidgetsInNegativeQuadrantsContainedInTheFilteringRectangle() {
        var filteringRectangle = FilteringRectangle.of(-100, -150, 0, 0).get().orElseThrow();

        var filteredWidgets = filteringRectangle.apply(List.of(
                Fixtures.WIDGET_MINUS_200_MINUS_150,
                Fixtures.WIDGET_MINUS_100_MINUS_100,
                Fixtures.WIDGET_MINUS_100_MINUS_150));

        assertThat(filteredWidgets)
                .hasSize(2)
                .containsExactlyInAnyOrder(Fixtures.WIDGET_MINUS_100_MINUS_100, Fixtures.WIDGET_MINUS_100_MINUS_150);
    }

    private interface Fixtures {

        Widget WIDGET_50_50 = Widget.newWidget(Point.of(0, 0), 1, 100, 100);
        Widget WIDGET_50_100 = Widget.newWidget(Point.of(0, 50), 2, 100, 100);
        Widget WIDGET_100_100 = Widget.newWidget(Point.of(50, 50), 3, 100, 100);

        Widget WIDGET_MINUS_200_MINUS_150 = Widget.newWidget(Point.of(-200, -150), 1, 100, 100);
        Widget WIDGET_MINUS_100_MINUS_150 = Widget.newWidget(Point.of(-100, -150), 2, 100, 100);
        Widget WIDGET_MINUS_100_MINUS_100 = Widget.newWidget(Point.of(-100, -100), 3, 100, 100);

    }

}
