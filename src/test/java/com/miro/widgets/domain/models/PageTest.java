package com.miro.widgets.domain.models;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PageTest {

    @Test
    void failToCreatePageWithoutWidgets() {
        assertThatThrownBy(() -> Page.builder()
                .offset(0)
                .limit(10)
                .total(250)
                .widgets(null)
                .build())
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void failToCreatePageWithNegativeTotalCount() {
        assertThatThrownBy(() -> Page.builder()
                .offset(0)
                .limit(10)
                .total(-1)
                .widgets(Fixtures.WIDGETS)
                .build())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failToCreatePageWithNegativeOffset() {
        assertThatThrownBy(() -> Page.builder()
                .offset(-1)
                .limit(10)
                .total(250)
                .widgets(Fixtures.WIDGETS)
                .build())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failToCreatePageWithZeroLimit() {
        assertThatThrownBy(() -> Page.builder()
                .offset(0)
                .limit(0)
                .total(250)
                .widgets(Fixtures.WIDGETS)
                .build())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failToCreatePageWithNegativeLimit() {
        assertThatThrownBy(() -> Page.builder()
                .offset(0)
                .limit(-1)
                .total(250)
                .widgets(Fixtures.WIDGETS)
                .build())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void successfullyCalculateNextOffset() {
        Page page = Page.builder()
                .offset(20)
                .limit(10)
                .total(250)
                .widgets(Fixtures.WIDGETS)
                .build();

        assertThat(page.getNextOffset())
                .isEqualTo(30);
    }

    @Test
    void successfullyCheckThatThereIsNoNextPage() {
        Page page = Page.builder()
                .offset(20)
                .limit(10)
                .total(30)
                .widgets(Fixtures.WIDGETS)
                .build();

        assertThat(page.hasNextPage())
                .isFalse();
    }

    @Test
    void successfullyCheckThatThereIsNextPage() {
        Page page = Page.builder()
                .offset(20)
                .limit(10)
                .total(31)
                .widgets(Fixtures.WIDGETS)
                .build();

        assertThat(page.hasNextPage())
                .isTrue();
    }

    private interface Fixtures {

        List<Widget> WIDGETS = List.of(Widget.newWidget(Point.of(1, 2), 1, 20, 20));

    }

}
