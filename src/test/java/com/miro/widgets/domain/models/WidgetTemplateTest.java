package com.miro.widgets.domain.models;

import com.miro.widgets.util.Predicates;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class WidgetTemplateTest {

    @Test
    void successfullyCreateWidgetTemplate() {
        var widgetTemplate = WidgetTemplate.of(2, 2, 1, 20, 20);

        assertThat(widgetTemplate.getId())
                .isNotNull();
        assertThat(widgetTemplate.getBottomLeftPoint())
                .isEqualTo(Point.of(2, 2));
    }

    @Test
    void successfullyCreateWidgetTemplateWithoutOrder() {
        var widgetTemplate = WidgetTemplate.of(2, 2, null, 20, 20);

        assertThat(widgetTemplate.getId())
                .isNotNull();
        assertThat(widgetTemplate.getOrder())
                .isEmpty();
    }

    @Test
    void failCreatingWidgetTemplateWithZeroHeight() {
        assertThatThrownBy(() -> WidgetTemplate.of(2, 2, 1, 0, 20))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failCreatingWidgetTemplateWithZeroWidth() {
        assertThatThrownBy(() -> WidgetTemplate.of(2, 2, 1, 20, 0))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failCreatingWidgetTemplateWithNegativeHeight() {
        assertThatThrownBy(() -> WidgetTemplate.of(2, 2, 1, -1, 20))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failCreatingWidgetTemplateWithNegativeWidth() {
        assertThatThrownBy(() -> WidgetTemplate.of(2, 2, 1, 20, -1))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void successfullyProduceWidgetFromWidgetTemplate() {
        var widgetTemplate = WidgetTemplate.of(2, 2, 1, 20, 20);

        var producedWidget = widgetTemplate.produce(0);

        assertThat(producedWidget)
                .isNotNull()
                .matches(Predicates.widgetTemplate(widgetTemplate));
    }

    @Test
    void successfullyProduceWidgetFromWidgetTemplateWithMaximumOrder() {
        var widgetTemplate = WidgetTemplate.of(2, 2, null, 20, 20);

        var producedWidget = widgetTemplate.produce(7);

        assertThat(producedWidget.getOrder())
                .isEqualTo(8);
    }

}
