package com.miro.widgets.domain.models;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class WidgetTest {

    @Test
    void failCreatingNewWidgetWithoutBottomLeftPoint() {
        assertThatThrownBy(() -> Widget.newWidget(null, 5, 20, 20))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void failCreatingNewWidgetWithZeroHeight() {
        assertThatThrownBy(() -> Widget.newWidget(Point.of(1, 1), 5, 0, 20))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failCreatingNewWidgetWithNegativeHeight() {
        assertThatThrownBy(() -> Widget.newWidget(Point.of(1, 1), 5, -1, 20))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failCreatingNewWidgetWithZeroWidth() {
        assertThatThrownBy(() -> Widget.newWidget(Point.of(1, 1), 5, 20, 0))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failCreatingNewWidgetWithNegativeWidth() {
        assertThatThrownBy(() -> Widget.newWidget(Point.of(1, 1), 5, 20, -1))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void successfullyBumpUpOrderOfWidget() {
        var widget = Widget.newWidget(Point.of(1, 1), 5, 20, 20);

        var bumpedUpWidget = widget.bumpUp();

        assertThat(bumpedUpWidget)
                .isEqualTo(widget.toBuilder().order(widget.getOrder() + 1).build());
    }

    @Test
    void successfullyCalculateTopRightPoint() {
        var widget = Widget.newWidget(Point.of(1, 1), 5, 20, 20);

        assertThat(widget.getTopRightPoint())
                .isEqualTo(Point.of(21, 21));
    }

}
