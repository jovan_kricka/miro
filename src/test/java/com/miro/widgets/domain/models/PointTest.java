package com.miro.widgets.domain.models;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PointTest {

    @Test
    void checkThatOnePointIsLeftOfAnotherPoint() {
        assertThat(Point.of(5, 5).isLeftOfOrAlignedWith(Point.of(6, 5)))
                .isTrue();
    }

    @Test
    void checkThatOnePointIsAlignedWithAnotherPointVertically() {
        assertThat(Point.of(5, 5).isLeftOfOrAlignedWith(Point.of(5, 15)))
                .isTrue();
    }

    @Test
    void checkThatOnePointIsRightOfAnotherPoint() {
        assertThat(Point.of(5, 5).isRightOfOrAlignedWith(Point.of(2, 5)))
                .isTrue();
    }

    @Test
    void checkThatOnePointIsLowerThanAnotherPoint() {
        assertThat(Point.of(5, 5).isLowerThanOrAlignedWith(Point.of(7, 15)))
                .isTrue();
    }

    @Test
    void checkThatOnePointIsAlignedWithAnotherPointHorizontally() {
        assertThat(Point.of(5, 5).isLowerThanOrAlignedWith(Point.of(7, 5)))
                .isTrue();
    }

    @Test
    void checkThatOnePointIsHigherThanAnotherPoint() {
        assertThat(Point.of(5, 5).isHigherThanOrAlignedWith(Point.of(7, 2)))
                .isTrue();
    }

}
