package com.miro.widgets;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("inMemory")
@SpringBootTest
class WidgetsApplicationTests {

	@Test
	void contextLoads() {
	}

}
